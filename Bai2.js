function pow(x, n) {
  var temp = x;
  for (var i = 1; i < n; i++) {
    x *= temp;
  }
  return x;
}

function bai2() {
  var x = document.getElementById("numx").value * 1;
  var n = document.getElementById("numn").value * 1;
  var sum = 0;
  for (var i = 1; i <= n; i++) {
    sum += pow(x, i);
  }
  document.getElementById("ket_qua_bai2").innerHTML = `Tổng là: ${sum}`;
  document.getElementById("ket_qua_bai2").style.backgroundColor = "#f8d7da";
  document.getElementById("ket_qua_bai2").style.padding = "10px";
}
